About Extract single author from WXR
=================

Extract the posts of a single author from a WordPress eXtended RSS (WXR) export file 
into a new WXR file.

For example, if you had a WXR export from a group blog, but only wanted to
import a single author's content into another blog, you could pass the WXR
through this script to extract only that author's content.

Caution: WordPress import files, especially partial ones like the output of
this script, are RUBBISH at importing attachments/media. You will probably want
to manually import any media.

Requirements
============

Python 3.

Usage
=====

    usage: extract-single-author-from-input-file.py [-h]
                                                    author_name inputfile
                                                    outputfile
    
    positional arguments:
      author_name  Username of author whose posts are to be extracted from file
      inputfile    Input WXR file containing WordPress posts
      outputfile   Output WXR file containing WordPress posts by author_name
    
    optional arguments:
      -h, --help   show this help message and exit

Author
======

Extract single author from WXR is by [Mary Gardiner](https://mary.gardiner.id.au/).

License
=======

Extract single author from WXR is available under the MIT License. See the LICENSE file for the
licence.
