#!/usr/bin/env python3

import argparse
import xml.etree.ElementTree as ET

CHANNEL = 'channel'
ITEM = 'item'
CREATOR = '{http://purl.org/dc/elements/1.1/}creator'
AUTHOR = '{http://wordpress.org/export/1.2/}author'
AUTHOR_LOGIN = '{http://wordpress.org/export/1.2/}author_login'

def find_author_in_children(parent, author_name, tag_name):
    right_author = False
    for child in parent:
        if child.tag == tag_name and child.text == author_name:
            return True
    return False

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('author_name', help="Username of author whose posts are to be extracted from file")
    parser.add_argument('inputfile', help="Input WXR file containing WordPress posts")
    parser.add_argument('outputfile', help="Output WXR file containing WordPress posts by author_name")

    args = parser.parse_args()

    tree = ET.parse(args.inputfile)
    root = tree.getroot()
    channel = tree.find(CHANNEL)

    for author_elem in channel.findall(AUTHOR):
        if not find_author_in_children(author_elem, args.author_name, AUTHOR_LOGIN):
            channel.remove(author_elem)

    for item in channel.findall(ITEM):
        if not find_author_in_children(item, args.author_name, CREATOR):
            channel.remove(item)

    tree.write(args.outputfile)

if __name__ == '__main__':
    main()
